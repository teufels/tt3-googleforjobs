[![VENDOR](https://img.shields.io/badge/vendor-teufels-blue.svg)](https://bitbucket.org/teufels/workspace/projects/TTER)
[![PACKAGE](https://img.shields.io/badge/package-tt3--googleforjobs-orange.svg)](https://bitbucket.org/teufels/tt3-googleforjobs/src/main/)
![version](https://img.shields.io/badge/version-1.1.*-yellow.svg?style=flat-square)
[![KEY](https://img.shields.io/badge/extension--key-tt3__googleforjobs-red.svg)](https://bitbucket.org/teufels/tt3-googleforjobs/src/main/)

[ ṯeufels ] Google for Jobs
==========
Render Google for Jobs structured data for static job pages

#### This version supports TYPO3
![TYPO3Version](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)
![TYPO3Version](https://img.shields.io/badge/13_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req teufels/tt3-googleforjobs`

***

### How to use
- Install with composer
- Import Static Template (before sitepackage)
- place plugin & enter data

***

### Migration from tt3_googleforjobs
1. in composer.json replace `tt3_googleforjobs` with `"tt3-googleforjobs":"^1.0"`
2. Composer update
3. Perform Upgrade Wizards "[teufels] Google for Jobs"
4. check & adjust be user group access rights

***

### Changelog
#### 1.1.x
- add support for TYPO3 v13
#### 1.0.x
- 1.0.0 intial from [hive_googleforjobs](https://bitbucket.org/teufels/hive_googleforjobs/src/)

***
⚠️ **DEPRECATION WARNING**
> [!CAUTION]
> **Deprecation: #105076 - Plugin content element and plugin sub types**
> 
> This plugin have been registered using the `list` content element and the plugin subtype `list_type` field.
> asd
> This functionality, the plugin content element (`list`) and the plugin sub types
field (:php:`list_type`) have been marked as deprecated in TYPO3 v13.4 and will
be removed in TYPO3 v14.0.
> 
> The recommended way to create a plugin is by using a dedicated
content type (`CType`) for each plugin.
> 
> ❗ this plugin may not receive an update to TYPO3 v14 and must then be replaced ❗