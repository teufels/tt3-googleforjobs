<?php
namespace Teufels\Tt3Googleforjobs\Controller;

use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\ImageService;

/*
 * This file is part of the "tt3_googleforjobs" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * JobController
 */
class JobController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * action renderStructureddata
     */
    public function renderStructureddataAction(): ResponseInterface
    {
        $aSettings = $this->settings;

        $GJTitle = isset($aSettings['oTitle']) ? $aSettings['oTitle'] : '';
        $GJDescription = isset($aSettings['oDescription']) ? $aSettings['oDescription'] : '';
        $GJEmploymentType = isset($aSettings['oEmploymentType']) ? $aSettings['oEmploymentType'] : '';
        $GJHiringOragnization = isset($aSettings['oHiringOrganization']) ? $aSettings['oHiringOrganization'] : '';
        $GJHiringOragnizationID = isset($aSettings['oHiringOrganizationID']) ? $aSettings['oHiringOrganizationID'] : '';
        $GJHiringOragnizationLogo = isset($aSettings['oHiringOrganizationLogo']) ? $aSettings['oHiringOrganizationLogo'] : '';
        $GJHiringOragnizationWebsite = isset($aSettings['oHiringOrganizationWebsite']) ? $aSettings['oHiringOrganizationWebsite'] : '';
        $GJJobLocationStreetAddress = isset($aSettings['oJobLocationStreetAddress']) ? $aSettings['oJobLocationStreetAddress'] : '';
        $GJJobLocationCity = isset($aSettings['oJobLocationCity']) ? $aSettings['oJobLocationCity'] : '';
        $GJJobLocationPostalCode = isset($aSettings['oJobLocationPostalCode']) ? $aSettings['oJobLocationPostalCode'] : '';
        $GJJobLocationRegion = isset($aSettings['oJobLocationRegion']) ? $aSettings['oJobLocationRegion'] : '';
        $GJJobLocationCountry = isset($aSettings['oJobLocationCountry']) ? $aSettings['oJobLocationCountry'] : '';
        $GJBaseSalaryActive = isset($aSettings['oBaseSalaryActive']) ? $aSettings['oBaseSalaryActive'] : '';
        $GJBaseSalaryCurrency = isset($aSettings['oBaseSalaryCurrency']) ? $aSettings['oBaseSalaryCurrency'] : '';
        $GJBaseSalaryValue = isset($aSettings['oBaseSalaryValue']) ? $aSettings['oBaseSalaryValue'] : '';
        $GJBaseSalaryUnitText = isset($aSettings['oBaseSalaryUnitText']) ? $aSettings['oBaseSalaryUnitText'] : '';
        $GJRemoteJobActive = isset($aSettings['oRemoteJobActive']) ? $aSettings['oRemoteJobActive'] : '';
        $GJRemoteJobALR = isset($aSettings['oRemoteJobALR']) ? $aSettings['oRemoteJobALR'] : '';
        $GJDatePostedTimestamp = isset($aSettings['oDatePosted']) ? $aSettings['oDatePosted'] : '';
        $GJDatePosted = $GJDatePostedTimestamp !== '' ? date("Y-m-d", $GJDatePostedTimestamp) : '';
        $GJValidTroughTimestamp = isset($aSettings['oValidThrough']) ? $aSettings['oValidThrough'] : '';
        $GJValidTrough = $GJValidTroughTimestamp !== '' ? date("Y-m-d", $GJValidTroughTimestamp) : '';

        //get absolutePath of $GJHiringOragnizationLogo
        $imageService = GeneralUtility::makeInstance(ImageService::class);
        $imagePath = $imageService->getImage($GJHiringOragnizationLogo,null,false)->getPublicUrl();
        $GJHiringOragnizationLogo_AbsolutePath = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/" .  $imagePath;

        $GoogleForJobs = array();
        $GoogleForJobs['@context'] = "https://schema.org/";
        $GoogleForJobs['@type'] = 'JobPosting';
        $GoogleForJobs['title'] = $GJTitle;
        $GoogleForJobs['description'] = $GJDescription;

        $GoogleForJobs['image'] = $GJHiringOragnizationLogo_AbsolutePath;
        $GoogleForJobs['employmentType'] = $GJEmploymentType;

        $GoogleForJobs['identifier']['@type'] = 'PropertyValue';
        $GoogleForJobs['identifier']['name'] = $GJHiringOragnization;
        if($GJHiringOragnizationID){
            $GoogleForJobs['identifier']['value'] = $GJHiringOragnizationID;
        }

        $GoogleForJobs['hiringOrganization']['@type'] = 'Organization';
        $GoogleForJobs['hiringOrganization']['name'] = $GJHiringOragnization;
        $GoogleForJobs['hiringOrganization']['sameAs'] = $GJHiringOragnizationWebsite;
        $GoogleForJobs['hiringOrganization']['logo'] = $GJHiringOragnizationLogo_AbsolutePath;

        $GoogleForJobs['jobLocation']['@type'] = 'Place';
        $GoogleForJobs['jobLocation']['address']['@type'] = 'PostalAddress';
        $GoogleForJobs['jobLocation']['address']['streetAddress'] = $GJJobLocationStreetAddress;
        $GoogleForJobs['jobLocation']['address']['addressLocality'] = $GJJobLocationCity;
        $GoogleForJobs['jobLocation']['address']['addressRegion'] = $GJJobLocationRegion;
        $GoogleForJobs['jobLocation']['address']['postalCode'] = $GJJobLocationPostalCode;
        $GoogleForJobs['jobLocation']['address']['addressCountry'] = $GJJobLocationCountry;


        if($GJBaseSalaryActive){
            $GoogleForJobs['baseSalary']['@type'] = 'MonetaryAmount';
            $GoogleForJobs['baseSalary']['currency'] = $GJBaseSalaryCurrency;
            $GoogleForJobs['baseSalary']['value']['@type'] = 'QuantitativeValue';
            $GoogleForJobs['baseSalary']['value']['value'] = $GJBaseSalaryValue;
            $GoogleForJobs['baseSalary']['value']['unitText'] = $GJBaseSalaryUnitText;
        }

        if($GJRemoteJobActive) {
            $GoogleForJobs['jobLocationType'] = 'TELECOMMUTE';
            $GoogleForJobs['applicantLocationRequirements']['@type'] = 'Country';
            $GoogleForJobs['applicantLocationRequirements']['name'] = $GJRemoteJobALR;
        }

        $GoogleForJobs['datePosted'] = $GJDatePosted;
        $GoogleForJobs['validThrough'] = $GJValidTrough;

        $jsonData = json_encode($GoogleForJobs, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

        // add script tag
        $sturcturedData = '<script type="application/ld+json">' . $jsonData . '</script>';

        $this->view->assign('data', $GoogleForJobs);
        $this->view->assign('structuredData', $sturcturedData);

        return $this->htmlResponse();
    }
}
