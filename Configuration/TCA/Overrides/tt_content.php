<?php
defined('TYPO3') or die();

$extensionName = strtolower('tt3_googleforjobs');
$pluginName = 'Teufelsgoogleforjobsrenderstructureddata';

$pluginSignature = str_replace('_', '', $extensionName) . '_' . str_replace('_', '', strtolower($pluginName));
$pluginIcon = 'tt3_googleforjobs_icon';

/***************
 * Plugin
 ***************/

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Tt3Googleforjobs',
    $pluginName,
    '[teufels] Google For Jobs TEST',
    $pluginIcon,
    'teufels',
    'Render Google for Jobs Structured Data'
);


$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes'][$pluginSignature] = $pluginIcon ;

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:'. $extensionName . '/Configuration/FlexForms/Config.xml'
);