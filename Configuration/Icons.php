<?php

declare(strict_types=1);

$iconList = [];
foreach (['tt3_googleforjobs_icon' => 'Plugin.svg',
 ] as $identifier => $path) {
    $iconList[$identifier] = [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:tt3_googleforjobs/Resources/Public/Icons/' . $path,
    ];
}

return $iconList;
