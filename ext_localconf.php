<?php
defined('TYPO3') or die();

call_user_func(function() {

    /**
     * Register icons
     */
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'tt3_googleforjobs_icon',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        [
            'source' => 'EXT:tt3_googleforjobs/Resources/Public/Icons/Plugin.svg',
        ]
    );


    /**
     * configure Plugin
     */
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Tt3Googleforjobs',
        'Teufelsgoogleforjobsrenderstructureddata',
        [Teufels\Tt3Googleforjobs\Controller\JobController::class => 'renderStructureddata'],
        // non-cacheable actions
        [Teufels\Tt3Googleforjobs\Controller\JobController::class => ''],
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_PLUGIN
    );


});